//
//  Tests.m
//  Tests
//
//  Created by Ryan Ackley on 2/22/17.
//  Copyright © 2017 Ryan Ackley. All rights reserved.
//
#include <stdlib.h>
#import <XCTest/XCTest.h>
#import <ACHeck21Checks/ACHApiClient.h>
#import <ACHeck21Checks/ACHConfiguration.h>
// load models
#import <ACHeck21Checks/ACHAttachment.h>
#import <ACHeck21Checks/ACHCheck.h>
#import <ACHeck21Checks/ACHCheckQueryResult.h>
#import <ACHeck21Checks/ACHCreateAttachmentParams.h>
#import <ACHeck21Checks/ACHCreateCheckParams.h>
#import <ACHeck21Checks/ACHCredentials.h>
#import <ACHeck21Checks/ACHResourceLink.h>
#import <ACHeck21Checks/ACHReturn.h>
#import <ACHeck21Checks/ACHServerException.h>
#import <ACHeck21Checks/ACHSettlement.h>
#import <ACHeck21Checks/ACHValidateCheckParams.h>
#import <ACHeck21Checks/ACHValidationResult.h>
// load API classes for accessing endpoints
#import <ACHeck21Checks/ACHChecksApi.h>
#import <ACHeck21Checks/ACHLoginApi.h>

@interface Tests : XCTestCase

@end

@implementation Tests

static NSString *clientid = nil;

- (void)setUp {
    [super setUp];
     //  (optional)
    NSString *path = [[NSBundle bundleForClass:[self class]] pathForResource:@"testconfig" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSString *username = (NSString*)[dict objectForKey:@"username"];
    NSString *password = (NSString*)[dict objectForKey:@"password"];
    NSString *url = (NSString*)[dict objectForKey:@"url"];
    clientid = (NSString*)[dict objectForKey:@"clientid"];
    
    
    ACHConfiguration *config = [ACHConfiguration sharedConfig];
    [config setUsername:username];
    [config setPassword:password];
    [config setHost:url];
    
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testValidateTxn{
    int checkNum = arc4random_uniform(10000);
    ACHValidateCheckParams* values = [[ACHValidateCheckParams alloc] init];
    [values setCheckNumber: [@(checkNum) stringValue]];
    [values setAmount:@100.0];
    [values setRoutingNumber:@"061103852"];
    [values setAccountNumber:@"11101013"];
    [values setIndividualName:@"Nobody J. Smith"];
    
    ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];
    
    __block BOOL waitingForBlock = YES;
    [apiInstance validateCheckWithClientId:clientid
                                    values:values
                         completionHandler: ^(ACHValidationResult* output, NSError* error) {
                             waitingForBlock = NO;
                             if (output) {
                                 if ([[output result] isEqualToString:@"Authorized"]){
                                     NSLog(@"Good transaction");
                                 }
                                 else{
                                     
                                     NSLog(@"Bad transaction");
                                     XCTAssert(NO);
                                 }
                             }
                             if (error) {
                                 NSLog(@"Error calling ACHChecksApi->validateCheck: %@", error);
                                 XCTAssert(NO);
                             }
                         }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

-(void)testGetReturnDetails{
    ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];
    
    __block BOOL waitingForBlock = YES;
    // Retrieve returns for a specific Check.
    [apiInstance getReturnsWithClientId:clientid
                             documentId:@91210010
                      completionHandler: ^(NSArray<ACHReturn>* output, NSError* error) {
                          waitingForBlock = NO;
                          if (output) {
                              XCTAssert(output.count > 0);
                              XCTAssert([[output[0] traceNumber] isEqualToString:@"123456780812665"]);
                              XCTAssert([[output[0] returnCode] isEqualToString:@"R01"]);
                              NSLog(@"%@", output);
                          }
                          if (error) {
                              XCTAssert(NO);
                              NSLog(@"Error calling ACHChecksApi->getReturns: %@", error);
                          }
                      }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

-(void)testGetSettlementDetails{
    ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];
    
    __block BOOL waitingForBlock = YES;
    // Retrieve returns for a specific Check.
    [apiInstance getSettlementsWithClientId:clientid
                             documentId:@93377801
                      completionHandler: ^(NSArray<ACHSettlement>* output, NSError* error) {
                          waitingForBlock = NO;
                          if (output) {
                              XCTAssert(output.count > 0);
                              XCTAssert([[output[0] traceNumber] isEqualToString:@"104000010098634"]);
                              NSLog(@"%@", output);
                          }
                          if (error) {
                              XCTAssert(NO);
                              NSLog(@"Error calling ACHChecksApi->getReturns: %@", error);
                          }
                      }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

- (void)testFindChecks {
    //NSTimeInterval monthInSec = 60 * 60 * 24 * 30;
    //NSDate *today = [NSDate ];
    //NSDate *monthAgo = [NSDate dateWithTimeIntervalSinceNow:-monthInSec];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"MM/dd/yyyy";
    NSDate *start = [formatter dateFromString:@"01/01/2016"];
    NSDate *end = [formatter dateFromString:@"01/31/2016"];
    ACHChecksApi *apiInstance = [[ACHChecksApi alloc] init];
    
    __block BOOL waitingForBlock = YES;
    
    [apiInstance getChecksWithClientId:clientid
                             startDate:start
                               endDate:end
                         currentStates:nil
                                offset:@0
                                 limit:@10
                     completionHandler: ^(ACHCheckQueryResult* output, NSError* error) {
                         waitingForBlock = NO;
                         if (output) {
                             NSLog(@"%@", output);
                             XCTAssertEqualObjects(output.recordsTotal, @12);
                         }
                         if (error) {
                             XCTAssert(NO);
                             NSLog(@"Error calling ACHChecksApi->getChecks: %@", error);
                         }
                     }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
}

- (void)testCreateCheck {
    ACHChecksApi *apiInstance = [[ACHChecksApi alloc] init];
    ACHCreateCheckParams *values = [[ACHCreateCheckParams alloc] init];
    
    NSString *frontPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"check1F" ofType:@"tif"];
    NSString *rearPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"check1B" ofType:@"tif"];
    NSString *dlPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"dl" ofType:@"tif"];
    NSString *frontEncoded = [[NSData dataWithContentsOfFile:frontPath] base64EncodedStringWithOptions:0];
    NSString *rearEncoded = [[NSData dataWithContentsOfFile:rearPath] base64EncodedStringWithOptions:0];
    NSString *dlEncoded = [[NSData dataWithContentsOfFile:dlPath] base64EncodedStringWithOptions:0];
    
    int checkNum = arc4random_uniform(10000);
    
    
    [values setAmount:@100.0];
    [values setMicr:nil];
    [values setAccountNumber: @"1234567890"];
    [values setAddenda:@[@"Test Addenda"]];
    [values setEntryClass:@"C21"];
    [values setIndividualName:@"Nobody J Smith"];
    [values setRoutingNumber:@"063100277"];
    [values setCheckNumber:[@(checkNum) stringValue]];
    [values setAccountType:@"Checking"];
    [values setRearImageEncoded:rearEncoded];
    [values setFrontImageEncoded:frontEncoded];
   // [values setAttachments:<#(NSArray<ACHCreateAttachmentParams> *)#>];
    
    ACHCreateAttachmentParams *attachment = [[ACHCreateAttachmentParams alloc] init];
    [attachment set_id:@"NY-123456789"];
    [attachment setType:@"DriversLicense"];
    [attachment setImageEncoded:dlEncoded];
    [values setAttachments:@[attachment]];
    
    __block BOOL waitingForBlock = YES;
    __block ACHCheck *check = nil;
    
    [apiInstance createCheckWithClientId:clientid
                                  values:values
                       completionHandler: ^(ACHCheck* output, NSError* error) {
                           waitingForBlock = NO;
                           if (output) {
                               check = output;
                               XCTAssert([check documentId] > @0);
                               XCTAssertEqualObjects([check accountNumber], @"1234567890");
                               XCTAssertEqualObjects([check routingNumber], @"063100277");
                               XCTAssertEqualObjects([check amount], @100.0);
                               XCTAssertEqualObjects([check checkNumber], [@(checkNum) stringValue]);
                               NSLog(@"%@", output);
                           }
                           if (error) {
                               XCTAssert(NO);
                               NSLog(@"Error calling ACHChecksApi->createCheck: %@", error);
                           }
                           
                       }];
    while(waitingForBlock) {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    }
    
    if (check != nil)
    {
        waitingForBlock = YES;
        [apiInstance getCheckWithClientId:clientid
                               documentId:[check documentId]
                        completionHandler: ^(ACHCheck* output, NSError* error) {
                            waitingForBlock = NO;
                            if (output) {
                                NSLog(@"%@", output);
                            }
                            if (error) {
                                XCTAssert(NO);
                                NSLog(@"Error calling ACHChecksApi->getCheck: %@", error);
                            }
                        }];
        while(waitingForBlock) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        }
    }
    
    if (check != nil)
    {
        waitingForBlock = YES;
        [apiInstance voidCheckWithClientId:clientid documentId:[check documentId] completionHandler:^(NSError *error) {
            waitingForBlock = NO;
            if (!error)
            {
                NSLog(@"Deleted");
            }
            else{
                XCTAssert(NO);
                NSLog(@"Error calling ACHChecksApi->voidCheck: %@", error);
            }
        }];
        while(waitingForBlock) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                     beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
        }
    }
    
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
