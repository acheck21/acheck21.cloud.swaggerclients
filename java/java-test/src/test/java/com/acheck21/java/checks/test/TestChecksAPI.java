package com.acheck21.java.checks.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.acheck21.checks.ApiClient;
import com.acheck21.checks.ApiException;
import com.acheck21.checks.Configuration;
import com.acheck21.checks.api.ChecksApi;
import com.acheck21.checks.auth.HttpBasicAuth;
import com.acheck21.checks.model.AccountType;
import com.acheck21.checks.model.AttachmentType;
import com.acheck21.checks.model.Check;
import com.acheck21.checks.model.CheckQueryResult;
import com.acheck21.checks.model.CreateAttachmentParams;
import com.acheck21.checks.model.CreateCheckParams;
import com.acheck21.checks.model.EntryClass;
import com.acheck21.checks.model.ModelReturn;
import com.acheck21.checks.model.Settlement;
import com.acheck21.checks.model.ValidateCheckParams;
import com.acheck21.checks.model.ValidationResponseType;
import com.acheck21.checks.model.ValidationResult;

public class TestChecksAPI {

	private static String username = System.getProperty("test.username");
    private static String password = System.getProperty("test.password");
    private static String clientID = System.getProperty("test.clientid");
    private static String host = System.getProperty("test.host");
    
	@Before
	public void setUp()
	{
		ApiClient defaultClient = Configuration.getDefaultApiClient();
//        
//        // Configure HTTP basic authorization: basicAuth
        HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
        basicAuth.setUsername(username);
        basicAuth.setPassword(password);
//        
        defaultClient.setBasePath(host);
	}
	
//	@Test
//	public void testClearant() throws IOException, ApiException
//	{
//		String clientId = "9900000778";
//
//		CreateCheckParams values = new CreateCheckParams(); // CreateCheckParams | 
//		values.setAccountNumber("1234567890"); 
//		values.setAccountType(AccountType.CHECKING); 
//		values.setAmount(new BigDecimal(100.0)); 
//		values.setCheckNumber("1006"); 
//		values.setEntryClass(EntryClass.WEB); 
//		values.setRoutingNumber("063100277");
//
//		ApiClient apiClient = Configuration.getDefaultApiClient(); 
//		apiClient.setBasePath("https://gateway.acheck21.com/GlobalGateway");
//		apiClient.setUsername("ryanackley+demo@gmail.com"); 
//		apiClient.setPassword("SoftArtisans1"); 
//		apiClient.setConnectTimeout(60000); 
//		apiClient.getHttpClient().setReadTimeout(60000, TimeUnit.MILLISECONDS); 
//		apiClient.getHttpClient().setWriteTimeout(60000, TimeUnit.MILLISECONDS);
//
//		ChecksApi apiInstance = new ChecksApi(apiClient); 
//		try
//		{
//			Check result = apiInstance.createCheck(clientId, values);
//			Check ch = apiInstance.getCheck(clientId, result.getDocumentId());
//			
//			System.out.println(result);
//		}
//		catch (Exception e)
//		{
//			int x= 0;
//		}
//				
//	}
	
	@Test
	public void testCheckAllOptions() throws IOException {
		String checkNumber = String.valueOf(((int)(Math.random() * 10000000)));
		ChecksApi apiInstance = new ChecksApi();
		String clientId = clientID;//"9900000778"; // String | 
		CreateCheckParams values = new CreateCheckParams(); // CreateCheckParams | 
		values.setAccountNumber("1234567890");
		values.setAccountType(AccountType.CHECKING);
		values.addAddendaItem("Test Addenda");
		values.setAmount(new BigDecimal(100.0));
		values.setCheckNumber(checkNumber);
		values.setEntryClass(EntryClass.C21);
		String frontImageEncoded = toBase64(getClass().getResourceAsStream("/check1F.tif"));
        String backImageEncoded = toBase64(getClass().getResourceAsStream("/check1B.tif"));
        values.setFrontImageEncoded(frontImageEncoded);
        values.setRearImageEncoded(backImageEncoded);
        values.setIndividualName("Nobody J Smith");
        values.setMicr(null);
        values.setRoutingNumber("063100277");
        
        String dlImageEncoded = toBase64(getClass().getResourceAsStream("/dl.tif"));
        
        CreateAttachmentParams attachment = new CreateAttachmentParams();
        attachment.setId("NY-123456789");
        attachment.setType(AttachmentType.DRIVERSLICENSE);
        attachment.setImageEncoded(dlImageEncoded);
        
        try {
            Check result = apiInstance.createCheck(clientId, values);
            Check check = apiInstance.getCheck(clientId, result.getDocumentId());
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling ChecksApi#createCheck");
            fail();
        }
	}
	
	@Test
	public void testGetChecks()
	{
		ChecksApi apiInstance = new ChecksApi();
		//long monthInMillis = 60L * 60L * 24L * 30L * 1000L;
		//Date now = new Date();
		//Date monthAgo = new Date(System.currentTimeMillis() - monthInMillis);
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(2015, 11, 31);
		Date start = cal.getTime();
		cal.set(2016, 0, 30);
		Date end = cal.getTime();
		List<String> currentStates = new ArrayList<String>();
		currentStates.add("Settled");
		currentStates.add("Settling");
		currentStates.add("Pending");
		currentStates.add("Deleted");
		currentStates.add("Returned");
		try {
			CheckQueryResult result = apiInstance.getChecks(clientID, start, end, currentStates, 0, 500);
			Assert.assertEquals(12, (int)result.getRecordsTotal());
			System.out.println(result);
		} catch (ApiException e) {
			fail();
		}
	}
	
	@Test
	public void testGetReturnDetails() throws ApiException
	{
		ChecksApi apiInstance = new ChecksApi();
		List<ModelReturn> returns = apiInstance.getReturns(clientID, 91210010l);
		assertTrue(returns.size() > 0);
		ModelReturn ret = returns.get(0);
		assertEquals(ret.getReturnMessage(), "Insufficient Funds");
	}
	
	@Test
	public void testGetSettlementDetails() throws ApiException
	{
		ChecksApi apiInstance = new ChecksApi();
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("US/Arizona"));
        cal.set(2016, 8, 20, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date start = cal.getTime();
        cal.set(2016, 8, 26, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date end = cal.getTime();
        
		CheckQueryResult result = apiInstance.getChecks(clientID, start, end, Arrays.asList(new String[]{"Settled"}), 0, 1000);
		assertEquals(43, (int)result.getRecordsTotal());
		for (Check ch : result.getChecks()) 
		{
			List<Settlement> settlements = apiInstance.getSettlements(clientID, ch.getDocumentId());
			assertTrue(settlements.size() > 0);
		}
		
		int x= 0;
	}
	@Test
	public void testValidateTransaction() throws ApiException
	{
		String checkNumber = String.valueOf(((int)(Math.random() * 10000000)));
		ValidateCheckParams params = new ValidateCheckParams();
		params.setAccountNumber("11101013");
		params.setCheckNumber(checkNumber);
		params.setRoutingNumber("061103852");
		params.setAmount(new BigDecimal(100));
		params.setIndividualName("Nobody J. Smith");
		
		ChecksApi apiInstance = new ChecksApi();
		ValidationResult response = apiInstance.validateCheck(clientID, params);
		if (response.getResult() == ValidationResponseType.AUTHORIZED)
		{
			System.out.println("Good transaction");
		}
		System.out.println(response);
		
	}
	
	private String toBase64(InputStream stream) throws IOException
	{
		byte[] byteArray = IOUtils.toByteArray(stream);
        String stream64 = Base64.getEncoder().encodeToString(byteArray);
        return stream64;
	}

}
